import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pve/network/access/domains/domainsAPI.dart';
import 'package:pve/network/access/ticketAPI.dart';
import 'package:pve/storage/storage.dart';

class LoginController extends GetxController {
  final DomainsAPI accessAPI = Get.find<DomainsAPI>();
  final TicketAPI ticketAPI = Get.find<TicketAPI>();
  TextEditingController baseurl = TextEditingController();
  TextEditingController domainsId = TextEditingController();
  TextEditingController username = TextEditingController();
  TextEditingController password = TextEditingController();
  var urlsEntries = <DropdownMenuEntry>[].obs;
  var domainsEntries = <DropdownMenuEntry>[].obs;
  var isLoading = false.obs;
  var domainsVal = "".obs;

  void reUrls() {
    urlsEntries.clear();
    urlsEntries.value
        .add(DropdownMenuEntry(value: "addurl", label: "login.addUrl".tr));
    print(PveStorage.Domains.length);
    for (var i = 0; i < PveStorage.Domains.length; i++) {
      var path = PveStorage.Domains.getAt(i)["host"] +
          ":" +
          PveStorage.Domains.getAt(i)["port"];
      urlsEntries.value.add(
        DropdownMenuEntry(value: path, label: path),
      );
    }
    urlsEntries.refresh();
  }

  @override
  void onReady() {
    super.onReady();
    print("onReady");
  }

  @override
  void refresh() {
    super.refresh();
  }

  @override
  Future<void> onInit() async {
    super.onInit();
    reUrls();
    // baseurl.value = PveStorage.Setting.get("baseurl");
    for (var i = 0; i < PveStorage.User.length; i++) {
      print(PveStorage.User.getAt(i));
    }
  }

  Future<void> getDomains() async {
    //清除数组
    domainsEntries.clear();
    var res = await accessAPI.getDomains();
    if (res.isOk) {
      for (var i in res.body["data"]) {
        domainsEntries.add(
          DropdownMenuEntry(value: i["type"], label: i["comment"]),
        );
      }
      domainsEntries.refresh();
    }
  }

  Future<bool> login(String username, password, realm) async {
    isLoading.value = true;
    var res = await ticketAPI
        .postTicket(TicketPOST(username, password, realm: realm));
    print(res.bodyString);
    isLoading.value = false;
    if (res.isOk) {
      var data = res.body["data"];
      PveStorage.User.put("username", data["username"]);
      PveStorage.User.put("ticket", data["ticket"]);
      PveStorage.User.put("user", data);
      return true;
    }
    return false;
  }
}

class LoginControllerBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DomainsAPI());
    Get.lazyPut(() => LoginController());
  }
}
