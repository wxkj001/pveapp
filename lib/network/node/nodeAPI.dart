import 'package:get/get_connect/http/src/response/response.dart';
import 'package:pve/network/network.dart';

class NodesAPI extends NetWork {
  Future<Response> getNodes() async => await get("/nodes");
}
