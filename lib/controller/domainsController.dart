import 'package:get/get.dart';
import 'package:pve/storage/storage.dart';

class DomainsController extends GetxController {
  var Host = "".obs;
  var Port = "".obs;
  var ListLength = PveStorage.Domains.length.obs;

  @override
  void onInit() {
    super.onInit();
    print(PveStorage.Setting.get("baseurl"));
  }
}

class DomainsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DomainsController());
  }
}
