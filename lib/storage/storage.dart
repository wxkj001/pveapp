import 'package:hive_flutter/hive_flutter.dart';

class PveStorage {
  static late final Box User;
  static late final Box Domains;
  static late final Box Setting;
  static bool _initialized = false;

  static Future<void> ensureInitialized() async {
    await Hive.initFlutter("hive");
    User = await Hive.openBox("pveUser");
    Domains = await Hive.openBox("pveURLS");
    Setting = await Hive.openBox("pveSetting");
  }
}
