import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pve/storage/storage.dart';

class AuthMiddleWare extends GetMiddleware {
  @override
  RouteSettings? redirect(String? route) {
    print("redirect");
    print("redirectDelegate");
    print(PveStorage.User.get("ticket"));
    if(PveStorage.User.get("ticket")==null){
       return const RouteSettings(name: "/login");
    }
    return super.redirect(route);
  }
}