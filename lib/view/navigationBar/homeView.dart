import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:pve/controller/navigationBar/homeController.dart';

class HomeView extends GetView<HomeController> {
  final selectedOption = "pve".obs; // 存储当前选中的选项
  List<String> options = ['pve', '节点2', '节点3'];
  List<Color> gradientColors = [
    Color(0xFF50E4FF),
    Color(0xFF2196F3),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text('home.title'.tr),
            Expanded(
              child: Center(
                child: Obx(() => DropdownButton<String>(
                      value: selectedOption.value, // 当前选中的选项
                      onChanged: (newValue) {
                        print(newValue);
                        selectedOption.value = newValue!;
                      },
                      items:
                          options.map<DropdownMenuItem<String>>((String value) {
                        // 创建下拉框选项列表
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value), // 显示的文本
                        );
                      }).toList(),
                    )),
              ),
            )
          ],
        ),
        /*actions: [
          Text("data")
        ],*/
      ),
      body: RefreshIndicator(
          onRefresh: _onRefresh,
          child: AnimationLimiter(
            child: ListView.builder(
                itemCount: 20,
                itemBuilder: (context, index) {
                  //滑动组件
                  return Slidable(
                    // Specify a key if the Slidable is dismissible.
                    key: const ValueKey(0),

                    // The start action pane is the one at the left or the top side.
                    startActionPane: ActionPane(
                      // A motion is a widget used to control how the pane animates.
                      motion: const ScrollMotion(),

                      // A pane can dismiss the Slidable.
                      dismissible: DismissiblePane(onDismissed: () {}),

                      // All actions are defined in the children parameter.
                      children: [
                        // A SlidableAction can have an icon and/or a label.
                        SlidableAction(
                          onPressed: doNothing,
                          backgroundColor: Color(0xFFFE4A49),
                          foregroundColor: Colors.white,
                          icon: Icons.delete,
                          label: 'Delete',
                        ),
                        SlidableAction(
                          onPressed: doNothing,
                          backgroundColor: Color(0xFF21B7CA),
                          foregroundColor: Colors.white,
                          icon: Icons.share,
                          label: 'Share',
                        ),
                      ],
                    ),
                    // The end action pane is the one at the right or the bottom side.
                    endActionPane: ActionPane(
                      motion: const ScrollMotion(),
                      children: [
                        SlidableAction(
                          // An action can be bigger than the others.
                          flex: 2,
                          onPressed: (context) => doNothing(context),
                          backgroundColor: Color(0xFF7BC043),
                          foregroundColor: Colors.white,
                          icon: Icons.settings,
                          label: '编辑',
                        ),
                        SlidableAction(
                          onPressed: (context) => doNothing(context),
                          backgroundColor: Color(0xFFFE4A49),
                          foregroundColor: Colors.white,
                          icon: Icons.delete,
                          // label: '删除',
                        ),
                      ],
                    ),

                    // The child of the Slidable is what the user sees when the
                    // component is not dragged.
                    child: ListTile(
                        leading: Text("ubuntu"),
                        title: Row(
                          children: [
                            // Text("ubuntu"),
                            Container(
                              width: 160,
                              height: 20,
                              child: LineChart(mainData()),
                            )
                          ],
                        )),
                  );
                }),
          )),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }

  /**
   * 下拉刷新方法,为list重新赋值
   */
  Future<Null> _onRefresh() async {
    await Future.delayed(Duration(seconds: 3), () {
      print('refresh');
    });
  }

  Future<dynamic> doNothing(context) async {
    print(context);
    return true;
  }

  //折线图配置
  LineChartData mainData() {
    return LineChartData(
      lineTouchData: LineTouchData(enabled: false),
      //禁止操作
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        horizontalInterval: 1,
        verticalInterval: 1,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: Colors.white10,
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: Colors.white10,
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: false,
      ),
      borderData: FlBorderData(
        show: false,
        border: Border.all(color: const Color(0xff37434d)),
      ),
      minX: 0,
      maxX: 11,
      minY: 0,
      maxY: 6,
      lineBarsData: [
        LineChartBarData(
          //最大值是10，最小是0
          spots: const [
            FlSpot(0, 1),
            FlSpot(2, 2),
            FlSpot(2.6, 2.6),
            FlSpot(4.9, 4.9),
            FlSpot(6.8, 10),
            FlSpot(8, 8),
            FlSpot(9.5, 9.5),
            FlSpot(11, 1.1),
          ],
          isCurved: true,
          gradient: LinearGradient(
            colors: gradientColors,
          ),
          barWidth: 1,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            gradient: LinearGradient(
              colors: gradientColors
                  .map((color) => color.withOpacity(0.3))
                  .toList(),
            ),
          ),
        ),
      ],
    );
  }
}
