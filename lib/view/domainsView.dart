import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:pve/controller/domainsController.dart';
import 'package:pve/storage/storage.dart';

class DomainsView extends GetView<DomainsController> {
  const DomainsView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("domains.title".tr),
      ),
      body: RefreshIndicator(
        onRefresh: _onRefresh,
        child: AnimationLimiter(
          child: Obx(
            () => ListView.builder(
              itemCount: controller.ListLength.value,
              itemBuilder: (context, index) {
                var path = PveStorage.Domains.getAt(index)["host"] +
                    ":" +
                    PveStorage.Domains.getAt(index)["port"];
                return Slidable(
                  endActionPane: ActionPane(
                    motion: const ScrollMotion(),
                    children: [
                      SlidableAction(
                        // An action can be bigger than the others.
                        flex: 2,
                        onPressed: (context) => doNothing(context),
                        backgroundColor: Color(0xFF7BC043),
                        foregroundColor: Colors.white,
                        icon: Icons.settings,
                        label: 'edit'.tr,
                      ),
                      SlidableAction(
                        onPressed: (context) {
                          PveStorage.Domains.deleteAt(index);
                          controller.ListLength--;
                        },
                        backgroundColor: Color(0xFFFE4A49),
                        foregroundColor: Colors.white,
                        icon: Icons.delete,
                      ),
                    ],
                  ),
                  child: ListTile(
                    onTap: () {
                      PveStorage.Setting.put("baseurl", path);
                      Get.back(result: {"path": path});
                    },
                    title: Text(path),
                  ),
                );
              },
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => openAdd(context),
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }

  Future<void> openAdd(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          TextEditingController host =
              TextEditingController(text: controller.Host.value);
          TextEditingController port =
              TextEditingController(text: controller.Port.value);
          return AlertDialog(
            content: SizedBox(
                height: 160,
                child: Column(
                  children: [
                    TextField(
                      controller: host,
                      decoration:
                          InputDecoration(hintText: "", labelText: "host".tr),
                    ),
                    TextField(
                      controller: port,
                      decoration: InputDecoration(
                          hintText: "443", labelText: "port".tr),
                    ),
                  ],
                )),
            actions: [
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text("cancel".tr),
              ),
              TextButton(
                onPressed: () {
                  print({"host": host.text, "port": port.text});
                  if (port.text == "") {
                    port.text = "443";
                  }
                  host.text = "https://${host.text}";
                  PveStorage.Domains.add(
                      {"host": host.text, "port": port.text});
                  controller.ListLength++;
                  Navigator.of(context).pop(true);
                },
                child: Text("confirm".tr),
              ),
            ],
          );
        });
  }

  /*
  * 下拉刷新方法,为list重新赋值
  */
  Future<Null> _onRefresh() async {
    await Future.delayed(Duration(seconds: 3), () {
      controller.ListLength.value = PveStorage.Domains.length;
    });
  }

  Future<dynamic> doNothing(context) async {
    openAdd(context);
    return true;
  }
}
