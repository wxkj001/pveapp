import 'package:get/get_connect/http/src/response/response.dart';
import 'package:pve/network/network.dart';

class TicketAPI extends NetWork {
  //Dummy. Useful for formatters which want to provide a login page.
  Future<Response> getTicket() async => await get("/access/ticket");

  //Create or verify authentication ticket.
  //登录
  Future<Response> postTicket(TicketPOST data) async =>
      await post("/access/ticket", data.toJson());
}

class TicketPOST {
  String password;
  String username;
  bool? newFormat = true;
  String? opt = "";
  String? path = "";
  String? privs = "";
  String? realm = "";
  String? tfaChallenge = "";

  TicketPOST(this.username, this.password,
      {this.newFormat,
      this.opt,
      this.path,
      this.privs,
      this.realm,
      this.tfaChallenge});

  Map<String, dynamic> toJson() {
    return {
      "password": password,
      "username": username,
      "new-format": newFormat??true?1:0,
      "realm": realm,
      // "opt": opt,
      // "path": path,
      // "privs": privs,
      // "tfa-challenge": tfaChallenge,
    };
  }
}
