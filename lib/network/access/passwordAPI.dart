import 'package:get/get_connect/http/src/response/response.dart';
import 'package:pve/network/network.dart';

class PasswordAPI extends NetWork {
  //Change user password.
  Future<Response> putPassword(String password, userid) async =>
      await put("/access/password", {"password": password, "userid": userid});
}
