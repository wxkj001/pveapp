import 'package:get/get_connect/http/src/response/response.dart';
import 'package:pve/network/network.dart';

class PermissionsAPI extends NetWork {
  // Retrieve effective permissions of given user/token
  // 检查token
  Future<Response> getPermissions(String? path, userid) async =>
      await get("/access/permissions", query: {"path": path, "userid": userid});
}
