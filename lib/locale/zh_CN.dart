import 'package:get/get_navigation/src/root/internacionalization.dart';

class zhCN extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        "zh_CH": {
          "edit": "编辑",
          "port": "端口号",
          "host": "IP/域名",
          "confirm": "确认",
          "cancel": "取消",
          "unauthorized": "登录失败，请重试",
          "domains.title": "选择地址",
          "login.title": "登录",
          "login.selectUrl": "选择地址",
          "login.domains": "选择领域",
          "login.addUrl": "添加地址",
          "login.username": "账号",
          "login.password": "密码",
          "login.submit": "登录",
          "login.language": "语言",
          "nav_bar.home": "首页",
          "nav_bar.my": "账户",
          "nav_bar.title": "pve",
          "home.title": "虚拟机"
        }
      };
}
