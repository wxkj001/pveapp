import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:pve/controller/navigationBar/homeController.dart';
import 'package:pve/network/access/accessAPI.dart';
import 'package:pve/view/navigationBar/homeView.dart';
import 'package:pve/view/navigationBar/myView.dart';

class NavigationBarController extends GetxController{
  final title = 'My Awesome View'.obs;
  final List<Widget> pages=[
    HomeView(),
    MyView()
  ];
  final currentIndex=0.obs;
}
class NavigationBarBindings extends Bindings{
  @override
  Future<void> dependencies() async {
    Get.put(NavigationBarController());
    Get.put(HomeController());
  }

}