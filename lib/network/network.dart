import 'package:get/get.dart';
import 'package:pve/storage/storage.dart';

class NetWork extends GetConnect {
  var BaseUrl="/api2/json";
  @override
  Future<Response<T>> post<T>(String? url, body, {String? contentType, Map<String, String>? headers, Map<String, dynamic>? query, Decoder<T>? decoder, Progress? uploadProgress}) {
    return super.post(getBaseurl(url!), body, contentType: contentType, headers: setHeaders(headers), query: query, decoder:decoder, uploadProgress: uploadProgress);
  }
  @override
  Future<Response<T>> get<T>(String url, {Map<String, String>? headers, String? contentType, Map<String, dynamic>? query, Decoder<T>? decoder}) {
    return super.get(getBaseurl(url), headers:setHeaders(headers), contentType:contentType, query:query, decoder:decoder);
  }
  @override
  Future<Response<T>> put<T>(String url, body, {String? contentType, Map<String, String>? headers, Map<String, dynamic>? query, Decoder<T>? decoder, Progress? uploadProgress}) {
    return super.put(getBaseurl(url), body, contentType:contentType, headers:setHeaders(headers), query:query, decoder:decoder, uploadProgress:uploadProgress);
  }
  @override
  Future<Response<T>> delete<T>(String url, {Map<String, String>? headers, String? contentType, Map<String, dynamic>? query, Decoder<T>? decoder}) {
    return super.delete(getBaseurl(url), headers:setHeaders(headers), contentType:contentType, query:query, decoder:decoder);
  }
  //重新组装URL，防止设置域名不生效
  String getBaseurl(String url){
    return "${PveStorage.Setting.get("baseurl")}$BaseUrl$url";
  }
  Map<String, String>? setHeaders(Map<String, String>? headers){
    headers?['Authorization'] = PveStorage.User.get("ticket");
    return headers;
  }
  @override
  void onInit() {
    //
    // All request will pass to jsonEncode so CasesModel.fromJson()
    // httpClient.defaultDecoder = CasesModel.fromJson;
    // var baseurl = PveStorage.Setting.get("baseurl");
    // httpClient.baseUrl = '';

    // It's will attach 'apikey' property on header from all requests
    httpClient.addRequestModifier<dynamic>((request) {
      return request;
    });
    httpClient.addResponseModifier((request, response){
      //TODO 这里会被执行很多次，暂时没有解决办法
      if(response.unauthorized){
        PveStorage.User.delete("ticket");
        Get.snackbar("401", "unauthorized".tr);
      }
      if (response.hasError){

      }
      return response;
    });
    //Autenticator will be called 3 times if HttpStatus is
    //HttpStatus.unauthorized
    httpClient.maxAuthRetries = 3;
    super.onInit();
  }
}
