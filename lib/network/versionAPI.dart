
import 'package:get/get_connect/http/src/response/response.dart';
import 'package:pve/network/network.dart';

//获取版本号
class VersionAPI extends NetWork{
  Future<Response> getVersion() async =>await get("/version");
}