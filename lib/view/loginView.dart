import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_view.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:pve/controller/loginController.dart';
import 'package:pve/storage/storage.dart';

class LoginView extends GetView<LoginController> {
  const LoginView({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: Text("login.title".tr),
      ),
      body: Center(
          child: Container(
        width: 200,
        height: 560,
        child: Obx(() => Column(
              children: [
                Row(
                  children: [
                    DropdownMenu(
                      width: 200,
                      initialSelection: controller.baseurl.text,
                      controller: controller.baseurl,
                      label: Text("login.selectUrl".tr),
                      dropdownMenuEntries: controller.urlsEntries.value,
                      onSelected: (v) async {
                        print(v);
                        if (v == "addurl") {
                          var data = await Get.toNamed("/domains");
                          controller.baseurl.text = data["path"];
                          controller.reUrls();
                        } else {
                          controller.baseurl.text = v;
                          PveStorage.Setting.put("baseurl", v);
                        }
                        await controller.getDomains();
                        controller.refresh();
                        print(controller.domainsEntries.value.asMap());
                      },
                    ),
                  ],
                ),
                Padding(
                    padding: EdgeInsets.only(top: 20),
                    child: SizedBox(
                      child: Obx(() => DropdownMenu(
                            width: 200,
                            initialSelection: "",
                            controller: controller.domainsId,
                            label: Text("login.domains".tr),
                            dropdownMenuEntries:
                                controller.domainsEntries.value,
                            onSelected: (v) async {
                              print(v);
                              controller.domainsVal.value = v;
                            },
                          )),
                    )),
                TextField(
                  controller: controller.username,
                  decoration: InputDecoration(
                      hintText: "", labelText: "login.username".tr),
                ),
                TextField(
                  obscureText: true,
                  controller: controller.password,
                  decoration: InputDecoration(
                      hintText: "", labelText: "login.password".tr),
                ),
                /* Padding(
                  padding: EdgeInsets.all(10),
                  child:
                ),*/
                Row(
                  children: [
                    TextButton(
                      child: Column(
                        children: [
                          const Icon(Icons.abc),
                          Text(
                            'login.language'.tr,
                            style: const TextStyle(fontSize: 10),
                          )
                        ],
                      ),
                      onPressed: () {
                        // 处理按钮点击事件
                      },
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                        padding: const EdgeInsets.all(16.0),
                        textStyle: const TextStyle(fontSize: 20),
                      ),
                      onPressed: () async {
                        if (controller.isLoading.isTrue) {
                          return;
                        }
                        var res=await controller.login(
                            controller.username.text,
                            controller.password.text,
                            controller.domainsVal.value);
                        if (res){
                          Get.offNamed("/");
                        }
                      },
                      child: controller.isLoading.isFalse
                          ? Text('login.submit'.tr)
                          : LoadingAnimationWidget.staggeredDotsWave(
                              color: Colors.purple, size: 20),
                    ),
                  ],
                )
              ],
            )),
      )),
    );
  }
}
