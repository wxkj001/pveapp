import 'package:get/get_connect/http/src/response/response.dart';
import 'package:pve/network/network.dart';

class DomainsAPI extends NetWork {
  Future<Response> getDomains() async => await get("/access/domains");

  Future<Response> postDomains(String realm, type) async =>
      await post("/access/domains", {"realm": realm, "type": type});
}

class DomainsRealmAPI extends NetWork {
  Future<Response> getDomainsRealm(String realm) async =>
      await get("/access/domains/$realm");

  Future<Response> putDomainsRealm(String realm, dynamic data) async =>
      await put("/access/domains/$realm", data);

  Future<Response> deleteDomainsRealm(String realm) async =>
      await delete("/access/domains/$realm");
}

class DomainsRealmSyncAPI extends NetWork {
  Future<Response> postDomainsRealm(String realm, dynamic data) async =>
      await post("/access/domains/$realm/sync", data);
}
