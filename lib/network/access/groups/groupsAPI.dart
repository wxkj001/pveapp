import 'package:get/get_connect/http/src/response/response.dart';
import 'package:pve/network/network.dart';

class GroupsAPI extends NetWork {
  Future<Response> getGroups() async => await get("/access/groups");

  Future<Response> postGroups(String groupid, comment) async =>
      await post("/access/groups", {"groupid": groupid, "comment": comment});
}
