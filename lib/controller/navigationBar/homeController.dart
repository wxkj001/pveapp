import 'package:get/get.dart';
import 'package:pve/network/node/nodeAPI.dart';
import 'package:pve/storage/storage.dart';

class HomeController extends GetxController{
  final NodesAPI nodesAPI=Get.find<NodesAPI>();

  getNodes() async {
    var res=await nodesAPI.getNodes();
    print(res.bodyString);
  }
  @override
  Future<void> onInit() async {
    // TODO: implement onInit
    super.onInit();
    // print(PveStorage.User.get("ticket"));
    getNodes();
  }
}